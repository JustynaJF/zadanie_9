#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

int main()
{
    struct Client *head;
    head = (struct Client*)malloc(sizeof(struct Client));
    head = NULL;
    
    int option = -1;
    int number = 0;
    char new_surname[30] = "";
    char d_surname[30] = "";

    while(option!=0)
    {
        printf("\nUzytkowniku! Co chcesz zrobic?\n");
        printf("1. Dodac klienta.\n");
        printf("2. Usunac klienta.\n");
        printf("3. Wyswietlic ilosc klientow.\n");
        printf("4. Wyswietlic nazwiska klientow.\n");
        printf("0. Zakonczyc program.\n");

        scanf("%i", &option);

        switch (option)
        {
            case 0:
    	        return 0;
    	        break;
            case 1:
                printf("Ilu klientow chcesz dodac? ");
                scanf("%d", &number);
                while(number != 0)
                {
                    printf("Podaj nazwisko klienta, ktorego chcesz dodac: ");
                    scanf("%s", new_surname);
                    push_back(&head, new_surname);
                    number--;  
                }
                break;   
            case 2:
                printf("\nPodaj nazwisko, ktore chcesz usunac z listy: ");
                scanf("%s", d_surname);
                pop_by_surname(&head, d_surname);
                break;
            case 3:
                printf("\nNa liscie jest %d osob\n", list_size(head));
                break;
            case 4:
                show_list(head);
                break;
        }
    }
}